/*
 * Copyright (C) 2014 Francesco Azzola
 *  Surviving with Android (http://www.survivingwithandroid.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package dubnio.com.cfe.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class ShakeEventManager implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private static final float ALPHA = 0.8f; // timeinterval / ( timeinterval + 200 delta prev. calculated)
    private static final int MOV_COUNTS = 3;
    private static final int MOV_THRESHOLD = 5;
    private static final int SHAKE_WINDOW_TIME_INTERVAL = 1000; // milliseconds
    // Gravity force on x,y,z axis
    private float gravity[] = new float[]{0.0f, 0.0f, 0.0f};

    private int counter;
    private long firstMovTime;
    private ShakeListener listener;


    public ShakeEventManager() {
    }

    public static interface ShakeListener {
        public void onShake();
    }

    public void setListener(ShakeListener listener) {
        this.listener = listener;
    }

    public void init(Context ctx) {
        mSensorManager = (SensorManager)  ctx.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void register() {
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregister()  {
        resetCounter();
        mSensorManager.unregisterListener(this);
    }
//    long di;
//    long df;
//    int evnt;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
//        if (evnt%2 == 0){
//           di =System.currentTimeMillis();
//            evnt++;
//        }
//        else{
//            df = System.currentTimeMillis();
//            evnt++;
//        }
//        Log.d("ShkMng", ":::delta::"+(df-di));


        //each time sensor changes goes here:
        float maxAcc = calcMaxAcceleration(sensorEvent);
        if (counter == 0) {
            counter ++;
            firstMovTime = System.currentTimeMillis();
        }
        //are we in window?
        Log.d("ShkMng","Interval "+(System.currentTimeMillis() - firstMovTime));
        if((System.currentTimeMillis() - firstMovTime) < SHAKE_WINDOW_TIME_INTERVAL)
        {
            // was the event strong enough
            if (maxAcc >= MOV_THRESHOLD) {
                Log.d("ShkMng", "Mov counter ["+counter+"] Acc["+maxAcc+"]");
                counter++;
                //get more time if mov_threshold was passed
                firstMovTime = System.currentTimeMillis();
                //movs reached?
                if(counter>MOV_COUNTS)
                {
                    resetCounter();
                    if (listener != null)
                        listener.onShake();
                }
            }
        }
        else{
            resetCounter();
        }
    }


    private float calcMaxAcceleration(SensorEvent event) {
        gravity[0] = calcGravityForce(event.values[0], 0);
        gravity[1] = calcGravityForce(event.values[1], 1);
        gravity[2] = calcGravityForce(event.values[2], 2);
//        Log.d("ShkMng", "GRAV ["+gravity[0]+"]["+gravity[1]+"]["+gravity[2]+"]");
        // Linear acceleration
        float accX = event.values[0] - gravity[0];
        float accY = event.values[1] - gravity[1];
        float accZ = event.values[2] - gravity[2];
//        Log.d("ShkMng", "LINEAR ["+accX+"]["+accY+"]["+accZ+"]");
        // Get max acceleration in any direction
        float max1 = Math.max(Math.abs(accX), Math.abs(accY));
        return Math.max(max1, Math.abs(accZ));
    }

    // Low pass filter to calc gravity acceleration
    private float calcGravityForce(float currentVal, int index) {
        return  ALPHA * gravity[index] + (1 - ALPHA) * currentVal;
    }


    private void resetCounter() {
        Log.d("ShkMng", "Reset all data");
        counter = 0;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
