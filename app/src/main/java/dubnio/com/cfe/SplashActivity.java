package dubnio.com.cfe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import dubnio.com.cfe.login.LoginUserActivity;


public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;
    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mRunnable = createRunnable();
        new Handler().postDelayed(mRunnable, SPLASH_TIME_OUT);
    }

    private Runnable createRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginUserActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        };
    }
}