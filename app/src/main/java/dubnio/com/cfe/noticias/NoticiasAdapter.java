package dubnio.com.cfe.noticias;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import dubnio.com.cfe.R;

/**
 * Created by citmusa on 10/6/14.
 */
public class NoticiasAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<Noticia> noticias;
    private TextView mTitle;
    private TextView mShortDesc;
    private ImageView mImage;
    private ProgressBar mProgressBar;

    public NoticiasAdapter(Context context, ArrayList<Noticia> noticias) {
        this.context = context;
        this.noticias = noticias;
    }

    @Override
    public int getCount() {
        return noticias.size();
    }

    @Override
    public Object getItem(int i) {
        return noticias.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if(view == null){
//            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//            convertView = mInflater.inflate(R.layout.noticia,null);
            view = LayoutInflater.from(context).inflate(R.layout.noticia, parent, false);
        }

        mTitle = (TextView) view.findViewById(R.id.noticia_title);
        mImage = (ImageView) view.findViewById(R.id.noticia_img);
        mShortDesc = (TextView) view.findViewById(R.id.noticia_shortdesc);
        mProgressBar = (ProgressBar) view.findViewById(R.id.noticia_loading);

        Noticia noticia = noticias.get(position);
        mTitle.setText(noticia.getTitle());
        mShortDesc.setText(noticia.getShort_desc());

        Picasso.with(context)
                .load(noticia.getImageUrl())
                .error(android.R.drawable.ic_menu_report_image)
                .into(mImage, new imageLoadedCallback(mProgressBar));

        return view;
    }




    private class imageLoadedCallback implements Callback {
        View view;
        public imageLoadedCallback(View view) {
            this.view = view;
        }

        @Override
        public void onSuccess() {
            view.setVisibility(View.GONE);
        }

        @Override
        public void onError() {
            view.setVisibility(View.GONE);
        }
    }
}
