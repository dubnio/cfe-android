package dubnio.com.cfe.noticias;


import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;

import java.util.ArrayList;

import dubnio.com.cfe.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class NoticiasFragment extends Fragment {


    private DynamicListView mListView;
    private NoticiasAdapter noticiasAdapter;

    public NoticiasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_noticias, container, false);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final ArrayList<Noticia> noticias = new ArrayList<Noticia>();
        Noticia uno = new Noticia(1,"Lorem ipsum","http://www.dubnio.com",
                "http://vivirsalud.imujer.com/sites/vivirsalud.imujer.com/files/imagecache/primera/Por-que-nos-damos-electricidad-1.jpg",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lacinia ligula ut mauris euismod, quis cursus nibh mollis. Duis in sollicitudin ligula. Quisque sed auctor lorem. Vivamus eget tristique est, vel lobortis urna. Aliquam volutpat risus lorem, at consectetur odio gravida ultrices.");
        Noticia dos = new Noticia(2,"Dolor sit amet","http://www.dubnio.com",
                "http://www.boomker.com/wp-content/uploads/2014/08/la-electricidad.jpg",
                "Suspendisse ornare ante turpis, eget luctus augue pharetra quis. Nulla ipsum augue, suscipit dignissim fermentum in, euismod a purus.");
        Noticia tres = new Noticia(3,"Consectetur adipiscing","http://www.dubnio.com",
                "http://www.edomexaldia.com.mx/wp-content/uploads/2014/08/cfe1.jpg",
                "Vivamus dapibus mauris id odio tincidunt sollicitudin. Maecenas rutrum porta leo, ut ornare massa dignissim non.");
        Noticia cuatro = new Noticia(3,"Duis lacinia","http://www.dubnio.com",
                "http://vivirsalud.imujer.com/sites/vivirsalud.imujer.com/files/imagecache/primera/Por-que-nos-damos-electricidad-1.jpg",
                "Praesent interdum orci et aliquam dictum. Aenean vel ex ligula. Maecenas sodales tellus pharetra velit suscipit rhoncus. In urna eros, pellentesque id nibh ut, ullamcorper ultrices velit. Pellentesque sed enim risus. Sed sed sagittis orci. Nam quis congue eros.");
        noticias.add(uno);
        noticias.add(dos);
        noticias.add(tres);
        noticias.add(cuatro);

        mListView = (DynamicListView) getActivity().findViewById(R.id.list_noticias);
        mListView.enableSwipeToDismiss(new OnDismissCallback() {
            @Override
            public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    noticias.remove(position);
                    noticiasAdapter.notifyDataSetChanged();
                }
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getActivity().getApplicationContext(),"voy a"+noticias.get(i).getUrl(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(noticias.get(i).getUrl()));
                startActivity(intent);
            }
        });

        noticiasAdapter = new NoticiasAdapter(getActivity().getApplicationContext(),noticias);
        AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(noticiasAdapter);
        animationAdapter.setAbsListView(mListView);
        mListView.setAdapter(animationAdapter);
    }
}
