package dubnio.com.cfe.reports;



import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import dubnio.com.cfe.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ReportesFragment extends Fragment {


    private ListView mListView;

    public ReportesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reportes, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mListView = (ListView) getActivity().findViewById(R.id.listView);
        ArrayList<Reporte> reportes = new ArrayList<Reporte>();

        Reporte uno = new Reporte("No hay luz en mi casa", "(#221552)", "Status: En Progreso", "Av. Tlacotalpan #53 -3A");
        Reporte dos = new Reporte("No hay luz en mi colonia", "(#221334)", "Status: En Progreso", "Av. Tlacotalpan #53 -3A");
        reportes.add(uno);
        reportes.add(dos);
        reportes.add(uno);
        reportes.add(dos);
        reportes.add(uno);
        reportes.add(dos);
        ReporteAdapter reporteAdapter = new ReporteAdapter(
                getActivity().getApplicationContext(),
                R.layout.reporte,
                reportes);
        mListView.setAdapter(reporteAdapter);
    }


}
