package dubnio.com.cfe.reports;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import dubnio.com.cfe.R;

/**
 * Created by citmusa on 10/8/14.
 */
public class ReporteAdapter extends ArrayAdapter<Reporte> {

    private Context context;
    private ArrayList<Reporte> reportes;

    public ReporteAdapter(Context context, int viewResource, ArrayList<Reporte> reportes) {
        super(context, viewResource, reportes);
        this.context = context;
        this.reportes = reportes;
    }

    static class ViewHolder{
        private TextView mTitle;
        private TextView mCode;
        private TextView mStatus;
        private TextView mAddress;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.reporte,parent,false);
            ViewHolder viewHolder = new ViewHolder();

            viewHolder.mTitle = (TextView) convertView.findViewById(R.id.textView);
            viewHolder.mCode = (TextView) convertView.findViewById(R.id.textView2);
            viewHolder.mStatus = (TextView) convertView.findViewById(R.id.textView3);
            viewHolder.mAddress = (TextView) convertView.findViewById(R.id.textView4);

            convertView.setTag(viewHolder);
        }

        Reporte reporte = reportes.get(position);
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.mTitle.setText(reporte.getTitle());
        holder.mCode.setText(reporte.getCode());
        holder.mStatus.setText(reporte.getStatus());
        holder.mAddress.setText(reporte.getAddress());

        return convertView;

    }


}
