package dubnio.com.cfe.reports;

/**
 * Created by citmusa on 10/8/14.
 */
public class Reporte {
    private String title;
    private String code;
    private String status;
    private String address;
    private String icon;

    public Reporte(String title, String code, String status, String address) {
        this.title = title;
        this.code = code;
        this.status = status;
        this.address = address;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    private String color;

}
