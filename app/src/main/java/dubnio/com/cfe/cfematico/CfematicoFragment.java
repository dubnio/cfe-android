package dubnio.com.cfe.cfematico;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import dubnio.com.cfe.R;
import dubnio.com.cfe.user.NavigationDrawer;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class CfematicoFragment extends Fragment {

    private static View view;
    private static GoogleMap mMap;
    private static Double latitude, longitude;

    public CfematicoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = (RelativeLayout) inflater.inflate(R.layout.fragment_cfematico, container, false);
        // Passing harcoded values for latitude & longitude.
        // This is just used to drop a Marker on the Map
        latitude = 19.4316;
        longitude = -99.1355;

        setUpMap(); // For setting up the MapFragment

        return view;
    }
    /***** Sets up the map if it is possible to do so *****/
    public static void setUpMap() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) NavigationDrawer.fragmentManager
                    .findFragmentById(R.id.cfematico_map)).getMap();
            if (mMap!=null)
            {
                completeSetupMap();
            }
        }
    }

    /**
     * For add markers or lines, add listeners or move the camera.
     */
    public static void completeSetupMap(){
        // For showing a move to my loction button
        mMap.setMyLocationEnabled(true);
        // For dropping a marker at a point on the Map
        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Zócalo").snippet("Main Center"));
        // For zooming automatically to the Dropped PIN Location
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
                longitude), 12.0f));

    }

    /**** The mapfragment's id must be removed from the FragmentManager
     **** or else if the same it is passed on the next time then
     **** app will crash ****/
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mMap != null) {
            NavigationDrawer.fragmentManager.beginTransaction()
                    .remove(NavigationDrawer.fragmentManager.findFragmentById(R.id.cfematico_map)).commit();
            mMap = null;
        }
    }


}
